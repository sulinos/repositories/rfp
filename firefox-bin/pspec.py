class inary:
    class source:
        name = "firefox-bin"
        homepage = "https://gitlab.com/sulinos/devel/inary"
        rfp= "This is a rfp package"

        class packager:
            name = "Ali Rıza KESKİN"
            email = "parduscix@yandex.ru"
        license = "GPLv3"
        isa = []
        summary = "Firefox browser binary package"
        description = "Prebuilt firefox"
        archive = [("SKIP","https://gitlab.com/sulincix/outher/-/raw/gh-pages/space"),
          ]
        builddependencies = []
        packages = ["package","package_docs" , "package_devel",
                    "package_pages", "package_32bit"]

    class package:
        name = "firefox-bin"
        runtimedependencies = []
        additionalfiles = [("firefox-bin.desktop","root","0755","/usr/share/applications/firefox-bin.desktop")]
        files = [("config", "/etc"),
                 ("localedata", "/usr/share/locale"),
                 ("info", "/usr/share/info"),
                 ("data", "/usr/share"),
                 ("data", "/data/app"),
                 ("executable", "/usr/bin"),
                 ("library", "/usr/lib"),
                 ("library", "/lib"),
                 ("library", "/usr/libexec"),
                 ("executable", "/bin")]

    class package_docs:
        name = "firefox-bin-docs"
        runtimedependencies = ["firefox-bin"]
        files = [("library", "/usr/share/doc/")]

    class package_pages:
        name = "firefox-bin-pages"
        runtimedependencies = ["firefox-bin"]
        files = [("library", "/usr/share/man/")]

    class package_32bit:
        name = "firefox-bin-32bit"
        runtimedependencies = []
        files = [("library", "/usr/lib32"),
                 ("library", "/lib32")]

    class package_devel:
        name = "firefox-bin-devel"
        runtimedependencies = []
        files = [("data", "/usr/lib/pkgconfig"),
                 ("header", "/usr/include")]

    class history:
        update = [
            ["2021-10-18", "999.9", "First release",
                "Ali Rıza KESKİN", "parduscix@yandex.ru"],
        ]


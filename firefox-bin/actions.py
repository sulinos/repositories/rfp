#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt
from inary.actionsapi import inarytools
from inary.actionsapi import shelltools as tools
from inary.actionsapi import get
#more information : https://gitlab.com/sulinos/inary/tree/master/inary/actionsapi

def setup():
    tools.system("wget 'https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64' -O firefox.tar.bz2")
    tools.system("tar -xvf firefox.tar.bz2")

def install():
    tools.cd("../firefox")
    tools.system("mkdir -p {}/data/app/system/firefox".format(get.installDIR()))
    tools.system("cp -prfv * {}/data/app/system/firefox".format(get.installDIR()))


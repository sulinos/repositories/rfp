#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt
from inary.actionsapi import inarytools
from inary.actionsapi import shelltools as tools
from inary.actionsapi import get
import os, subprocess
#more information : https://gitlab.com/sulinos/inary/tree/master/inary/actionsapi
def setup():
    tools.cd("DEBS")
    for file in os.listdir():
        tools.system("ar -x {}".format(file))
        tools.system("tar -xf data.tar.xz")
        tools.system("rm -f debian-binary data.tar.xz control.tar.gz")
def reling(file):
    link=subprocess.getoutput("readlink {}".format(file)).replace("/opt","/data/app/system")
    os.unlink(file)
    tools.system("ln -s {} {}".format(link,file))

def build():
    tools.cd("DEBS")
    # Remove shitty mime infos
    tools.system("rm -rf usr/share/mimeinfo/libreoffice*")
    tools.cd("usr/share/applications")
    for file in os.listdir():
        reling(file)
    tools.cd("../../")
    tools.system("mv local/* ./")
    tools.system("rmdir local")
    for file in os.listdir("bin"):
        reling("bin/{}".format(file))

def install():
    tools.cd("DEBS")
    tools.system("mkdir -p {}/data/app/system/".format(get.installDIR()))
    tools.system("cp -prf usr {}".format(get.installDIR()))
    tools.system("cp -prf opt/* {}/data/app/system/".format(get.installDIR()))


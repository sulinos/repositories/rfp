We must use this repository for new package requests
[Create issue](https://gitlab.com/sulinos/repositories/rfp/-/issues)


[Telegram Group](https://t.me/sulinos)

#Hot to add this repository on your system

0- install latest inary (develop branch) from source code

1- run `inary ar rfp-repo https://gitlab.com/sulinos/repositories/rfp/-/raw/master/inary-index.xml`

2- run `inary ur`

3- you can install package from rfp with `inary em [pkgname]` command

#How to create new package

1- Source code avaiable for compiling (Makefile Meson Cmake Qmake build.sh ... required)

2- PSPEC creating: [PSPEC template](https://github.com/sulincix/pspec-template)

3- Dependencies found and building

4- Send pull request to [rfp page] (https://gitlab.com/sulinos/repositories/rfp/-/merge_requests)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from inary.actionsapi import autotools
from inary.actionsapi import shelltools
from inary.actionsapi import get


def install():
    shelltools.system("mkdir -p {}/usr/src/rtl8821ce-v5.5.2_34066.20200325".format(get.installDIR()))
    shelltools.system("cp -prfv * {}/usr/src/rtl8821ce-v5.5.2_34066.20200325".format(get.installDIR()))

import os

def postInstall():
    os.system("/usr/src/rtl8821ce-v5.5.2_34066.20200325/dkms-install.sh >/dev/stderr")
    
def preRemove():
    os.system("/usr/src/rtl8821ce-v5.5.2_34066.20200325/dkms-remove.sh >/dev/stderr")
